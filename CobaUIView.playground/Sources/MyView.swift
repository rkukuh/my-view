import UIKit

public class MyView: UIView {
    
    var viewHeight: CGFloat = 0
    var viewWidth: CGFloat = 0
    
    public init(height: CGFloat) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: height * 0.55, height: height))
        
        viewHeight = frame.height
        viewWidth = frame.width
        
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

